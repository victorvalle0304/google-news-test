import React from "react"
import "../styles/card/card_styles.css"

export const Card = props =>{
  const {urlToImage, source, title, description, publishedAt} = props 
  let currentTimeMs =  Date.now() 
  
  let time = Date.parse(publishedAt)
  let diff = Math.ceil((currentTimeMs - time) / (1000*60*60)) 
  

  return(
    <div className ="newCard">            
      <img src={urlToImage} alt="newImg"></img>
      <h2>{title}</h2>  
      <h3>{source.name}</h3>       
      <h4>{description}</h4>   
      <p>Published at: {publishedAt}</p>        
      <p>{diff} hours posted</p>      
      
    </div>
  );
}