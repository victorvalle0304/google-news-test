import React from "react"
import { Link } from "gatsby"
import { Card } from "../components/Card"
import { getNews } from "../services/googleNewsApi"
import { useState, useEffect } from "react"
import "../styles/board/board_styles.css"

export default function Home() {
  const [hasError, setErrors] = useState(false)
  const [gNew, setGNew] = useState([])


  async function fetchData() {
    try {
      const res = await getNews()
      const { articles } = res
      setGNew(articles)
    } catch (error) {
      setErrors(error)
    }
  }

  useEffect(() => {
    if (!gNew.length) {
      fetchData();
    }
  }, [gNew])


  if (hasError) {
    return <p>"Something error ocurred"</p>
  }

  const gNewsList = gNew.map(({ urlToImage, source, title, description, publishedAt }) => (
    <li key={title}>
      <Card
        urlToImage={urlToImage}
        source={source}
        title={title}
        description={description}
        publishedAt={publishedAt}
      >

      </Card>

    </li>
  ))

  return (
    <div className="mainDiv">
      <a href="/"  >
        Refresh
      </a>
      <ul>
        {gNewsList}
      </ul>
    </div>
  );




}
